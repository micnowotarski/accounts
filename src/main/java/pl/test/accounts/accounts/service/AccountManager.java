package pl.test.accounts.accounts.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import pl.test.accounts.accounts.domain.Account;
import pl.test.accounts.accounts.dto.AccountDto;
import pl.test.accounts.accounts.dto.CreateAccountDto;
import pl.test.accounts.accounts.exception.CantCreateAccountException;
import pl.test.accounts.accounts.exception.UserNotExisitsException;
import pl.test.accounts.accounts.factory.AccountFactory;
import pl.test.accounts.accounts.mapper.AccountMapper;
import pl.test.accounts.accounts.repository.AccountRepository;

@Slf4j
@Service
public class AccountManager {

    @Autowired
    private AccountValidationService accountValidationService;
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AccountFactory accountFactory;

    public Account getAccountByPesel(String pesel){
        Account account = accountRepository.getAccountByPesel(pesel);

        if(account != null){
            log.info("Loaded data for user {}", pesel);
            return account;
        } else {
            log.warn("Can't load data for user {}. User not exists", pesel);
            throw new UserNotExisitsException();
        }
    }

    public Account createAccount(CreateAccountDto createAccountDto) {
        if (accountValidationService.canCreateAccount(createAccountDto.getPesel())) {
            Account newAccount = accountFactory.create(createAccountDto);
            accountRepository.createAccount(newAccount);
            log.info("Created account for user {}", createAccountDto.getPesel());
            return newAccount;
        } else {
            log.warn("Can't create account for user {}. Account exists or is not an adult", createAccountDto.getPesel());
            throw new CantCreateAccountException();
        }
    }

}
