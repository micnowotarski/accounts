package pl.test.accounts.accounts.dto;

import lombok.Getter;
import lombok.Setter;
import pl.test.accounts.accounts.dto.common.BaseDto;
import pl.test.accounts.accounts.enumeration.Currency;

import java.math.BigDecimal;

@Getter
@Setter
public class ConvertCurrenciesDto extends BaseDto {
    private String pesel;
    private Currency currencyFrom;
    private Currency currencyTo;
    private BigDecimal amountTo;
}
