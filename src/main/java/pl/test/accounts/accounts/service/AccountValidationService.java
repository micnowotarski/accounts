package pl.test.accounts.accounts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.test.accounts.accounts.domain.Account;
import pl.test.accounts.accounts.repository.AccountRepository;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Service
public class AccountValidationService {

    private static final int ADULT_YEARS = 18;

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private PeselParser peselParser;


    public boolean canCreateAccount(String pesel){
        return accountNotExistsInRepository(pesel) && isAdult(pesel);
    }

    private boolean accountNotExistsInRepository(String pesel){
        Account accountFromRepository = accountRepository.getAccountByPesel(pesel);
        return accountFromRepository == null;
    }

    private boolean isAdult(String pesel){
        LocalDate birthDate = peselParser.parsePeselBirthDate(pesel);
        LocalDate minAdultBirthDay = LocalDate.now().minus(ADULT_YEARS, ChronoUnit.YEARS);
        return birthDate.isBefore(minAdultBirthDay);
    }
}
