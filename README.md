# README #
Test application for the recruitment process.

### Getting started ###
* Build:  
`mvn clean install`
* Run:  
`mvn spring-boot:run`  
application is running on `localhost:11001`

### Usage ###
* Create new account:  
`POST: localhost:11001/api/accounts/create`
* Get account data:  
`GET: localhost:11001/api/accounts/account/{pesel}`  
* Currency conversion:  
`POST: localhost:11001/api/accounts/convert`  

### Contact ###
Michał Nowotarski  
micnowotarski@gmail.com
