package pl.test.accounts.accounts.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.test.accounts.accounts.enumeration.Currency;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Subaccount {
    private Currency currencyCode;
    private BigDecimal balance;
}
