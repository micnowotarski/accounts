package pl.test.accounts.accounts.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.test.accounts.accounts.domain.Account;
import pl.test.accounts.accounts.dto.AccountDto;
import pl.test.accounts.accounts.dto.CreateAccountDto;

@Mapper(componentModel = "spring", uses = SubaccountMapper.class)
public interface AccountMapper {
    AccountDto map(Account account);

    @Mapping(target = "subaccounts", ignore = true)
    Account map(CreateAccountDto createAccountDto);
}
