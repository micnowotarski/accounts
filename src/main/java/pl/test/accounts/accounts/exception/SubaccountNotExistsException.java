package pl.test.accounts.accounts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="Requested subaccount not exists")
public class SubaccountNotExistsException extends RuntimeException{
}
