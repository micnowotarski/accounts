package pl.test.accounts.accounts.dto.rates;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class RateDto {
    private String no;
    private LocalDate effectiveDate;
    private BigDecimal mid;
}
