package pl.test.accounts.accounts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FAILED_DEPENDENCY, reason="Can't download exchange rate")
public class NoExchangeRateInApiException extends RuntimeException{
}
