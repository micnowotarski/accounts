package pl.test.accounts.accounts.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
public class PeselParser {

    private static final int MONTH_MULTIPLICATION = 20;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    public  LocalDate parsePeselBirthDate(String pesel){
        String yearString = pesel.substring(0, 2);
        String monthString = pesel.substring(2, 4);
        String birthDateString = getYearWithMonth(yearString, monthString) + pesel.substring(4, 6);
        log.debug("String {} is date {}", pesel.substring(0,6), birthDateString);
        return LocalDate.parse(birthDateString, formatter);
    }

    private String getYearWithMonth(String yearString, String monthString){
        int month = Integer.parseInt(monthString);
        if(is1800(month)){
            yearString = "18" + yearString;
        } else if(is1900(month)){
            yearString = "19" + yearString;
        } else if (is2000(month)){
            yearString = "20" + yearString;
        } else if (is2100(month)){
            yearString = "21" + yearString;
        }
        month = month % 20;
        monthString = month < 10 ? "0" + String.valueOf(month) : String.valueOf(month);
        return yearString + monthString;
    }

    private boolean is1800(int month){
        return month >= 81 && month <= 92;
    }

    private boolean is1900(int month){
        return month >= 1 && month <= 12;
    }

    private boolean is2000(int month){
        return month >= 21 && month <= 32;
    }

    private boolean is2100(int month){
        return month >= 41 && month <= 52;
    }
}
