package pl.test.accounts.accounts.repository.database;

import org.springframework.stereotype.Component;
import pl.test.accounts.accounts.domain.Account;

import java.util.HashMap;
import java.util.Map;

@Component
public class UserDatabase {
    private Map<String, Account> users = new HashMap<>();

    public Account createUser(Account account){
        users.put(account.getPesel(), account);
        return account;
    }

    public Account getUser(String pesel){
        return users.get(pesel);
    }
}
