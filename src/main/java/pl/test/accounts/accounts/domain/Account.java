package pl.test.accounts.accounts.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private String name;
    private String surname;
    private String pesel;
    private List<Subaccount> subaccounts;
}
