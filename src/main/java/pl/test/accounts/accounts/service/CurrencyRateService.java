package pl.test.accounts.accounts.service;

import java.math.BigDecimal;

public interface CurrencyRateService {
    BigDecimal getRateForCurrency(String currency);
}
