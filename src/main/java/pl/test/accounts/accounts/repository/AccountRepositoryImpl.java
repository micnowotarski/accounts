package pl.test.accounts.accounts.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.test.accounts.accounts.domain.Account;
import pl.test.accounts.accounts.repository.database.UserDatabase;

@Component
public class AccountRepositoryImpl implements AccountRepository {

    @Autowired
    private UserDatabase userDatabase;

    @Override
    public Account getAccountByPesel(String pesel) {
        return userDatabase.getUser(pesel);
    }

    @Override
    public Account createAccount(Account account) {
        return userDatabase.createUser(account);
    }
}
