package pl.test.accounts.accounts.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.test.accounts.accounts.domain.Account;
import pl.test.accounts.accounts.domain.Subaccount;
import pl.test.accounts.accounts.dto.ConvertCurrenciesDto;
import pl.test.accounts.accounts.enumeration.Currency;
import pl.test.accounts.accounts.exception.NoSufficientMoneyOnFromAccountException;
import pl.test.accounts.accounts.exception.SubaccountNotExistsException;
import pl.test.accounts.accounts.repository.AccountRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Slf4j
@Service
public class MoneyConversionService {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private CurrencyRateService currencyRateService;

    private static final int SUBACCOUNT_AMOUNT_SAME_AS_CONVERSION = 0;

    public void convertCurrencies(ConvertCurrenciesDto convertRequest){
        Account account = accountRepository.getAccountByPesel(convertRequest.getPesel());
        log.info("User {} started conversion of {} {} from {} account",
                convertRequest.getPesel(), convertRequest.getAmountTo(), convertRequest.getCurrencyTo(), convertRequest.getCurrencyFrom());
        Subaccount subaccountTo = getSubaccount(account, convertRequest.getCurrencyTo());
        Subaccount subaccountFrom = getSubaccount(account, convertRequest.getCurrencyFrom());
        BigDecimal amountOnFrom = getConversionAmount(convertRequest.getAmountTo(), subaccountTo, subaccountFrom);
        performConversion(convertRequest.getAmountTo(), amountOnFrom, subaccountTo, subaccountFrom, convertRequest.getPesel());
    }

    private Subaccount getSubaccount(Account account, Currency currency){
        return account.getSubaccounts().stream()
                .filter(sa -> sa.getCurrencyCode().equals(currency))
                .findFirst()
                .orElseThrow(SubaccountNotExistsException::new);
    }

    private BigDecimal getConversionAmount(BigDecimal amountTo, Subaccount subaccountTo, Subaccount subaccountFrom){
        boolean accountToIsPln = subaccountTo.getCurrencyCode() == Currency.PLN;
        Currency currencyToGetRate = accountToIsPln ? subaccountFrom.getCurrencyCode() : subaccountTo.getCurrencyCode();
        BigDecimal rate = currencyRateService.getRateForCurrency(currencyToGetRate.toString());
        log.info("Conversion rate for {}: {}", currencyToGetRate, rate);
        return accountToIsPln ? amountTo.divide(rate, 2, RoundingMode.UP) : amountTo.multiply(rate).setScale(2, RoundingMode.UP);
    }

    private void performConversion(BigDecimal amountTo, BigDecimal amountFrom, Subaccount subaccountTo, Subaccount subaccountFrom, String pesel){
        log.debug("Trying to convert {} {} to {} {}",
                amountFrom, subaccountFrom.getCurrencyCode(), amountTo, subaccountTo.getCurrencyCode());
        if (subaccountFrom.getBalance().compareTo(amountFrom) >= SUBACCOUNT_AMOUNT_SAME_AS_CONVERSION){
            subaccountFrom.setBalance(subaccountFrom.getBalance().subtract(amountFrom));
            subaccountTo.setBalance(subaccountTo.getBalance().add(amountTo));
            log.info("Converted {} {} to {} {}",
                    amountFrom, subaccountFrom.getCurrencyCode(), amountTo, subaccountTo.getCurrencyCode());
        } else {
            log.warn("User {} has no sufficient money on {} account to get {} {}",
                    pesel, subaccountFrom.getCurrencyCode(), amountTo, subaccountTo.getCurrencyCode());
            throw new NoSufficientMoneyOnFromAccountException();
        }
    }
}
