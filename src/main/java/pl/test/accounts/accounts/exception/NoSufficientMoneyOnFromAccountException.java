package pl.test.accounts.accounts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="User has no sufficient money on account to perform a conversion")
public class NoSufficientMoneyOnFromAccountException extends RuntimeException{
}
