package pl.test.accounts.accounts.dto;

import lombok.Getter;
import lombok.Setter;
import pl.test.accounts.accounts.dto.common.BaseDto;

import java.util.List;

@Getter
@Setter
public class AccountDto extends BaseDto {
    private String name;
    private String surname;
    private String pesel;
    private List<SubaccountDto> subaccounts;
}
