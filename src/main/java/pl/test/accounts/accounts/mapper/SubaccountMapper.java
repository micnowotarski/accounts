package pl.test.accounts.accounts.mapper;

import org.mapstruct.Mapper;
import pl.test.accounts.accounts.domain.Subaccount;
import pl.test.accounts.accounts.dto.SubaccountDto;

@Mapper(componentModel = "spring")
public interface SubaccountMapper {
    SubaccountDto map(Subaccount subaccount);
}
