package pl.test.accounts.accounts.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.test.accounts.accounts.domain.Account;
import pl.test.accounts.accounts.dto.AccountDto;
import pl.test.accounts.accounts.dto.ConvertCurrenciesDto;
import pl.test.accounts.accounts.dto.CreateAccountDto;
import pl.test.accounts.accounts.mapper.AccountMapper;
import pl.test.accounts.accounts.service.AccountManager;
import pl.test.accounts.accounts.service.MoneyConversionService;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/accounts")
public class AccountApiController {

    @Autowired
    private AccountManager accountManager;
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private MoneyConversionService moneyConversionService;

    @GetMapping(value = "/account/{pesel}")
    public AccountDto getAccount(@PathVariable String pesel){
        Account account = accountManager.getAccountByPesel(pesel);
        return accountMapper.map(account);
    }

    @PostMapping(value = "/create")
    public AccountDto createAccount(@Valid @RequestBody CreateAccountDto createAccountDto) {
        Account newAccount = accountManager.createAccount(createAccountDto);
        return accountMapper.map(newAccount);
    }

    @PostMapping(value = "/convert")
    public AccountDto convertCurrencies(@RequestBody ConvertCurrenciesDto convertCurrenciesDto) {
        moneyConversionService.convertCurrencies(convertCurrenciesDto);
        return getAccount(convertCurrenciesDto.getPesel());
    }

}
