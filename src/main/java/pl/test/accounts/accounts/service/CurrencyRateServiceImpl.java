package pl.test.accounts.accounts.service;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.test.accounts.accounts.dto.rates.ExchangeRatesSeriesDto;
import pl.test.accounts.accounts.dto.rates.RateDto;
import pl.test.accounts.accounts.exception.NoExchangeRateInApiException;

import java.math.BigDecimal;
import java.time.Duration;

@Service
public class CurrencyRateServiceImpl implements CurrencyRateService {

    private static final String RATE_API_URL = "http://api.nbp.pl/api/exchangerates/rates/a/";
    private static final int DEFAULT_TIMEOUT_MS = 5000;

    public BigDecimal getRateForCurrency(String currency){
        RestTemplate restTemplate = new RestTemplateBuilder()
                .setConnectTimeout(Duration.ofMillis(DEFAULT_TIMEOUT_MS))
                .setReadTimeout(Duration.ofMillis(DEFAULT_TIMEOUT_MS))
                .build();
        ExchangeRatesSeriesDto exchangeRatesSeries = restTemplate.getForObject(RATE_API_URL + currency, ExchangeRatesSeriesDto.class);
        RateDto rate = exchangeRatesSeries.getRates().stream().findFirst().orElseThrow(NoExchangeRateInApiException::new);
        return rate.getMid();
    }
}
