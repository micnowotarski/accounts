package pl.test.accounts.accounts.dto.rates;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ExchangeRatesSeriesDto {
    private String table;
    private String currency;
    private String code;
    private List<RateDto> rates;
}
