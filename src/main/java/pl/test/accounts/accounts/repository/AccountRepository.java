package pl.test.accounts.accounts.repository;

import pl.test.accounts.accounts.domain.Account;

public interface AccountRepository {
    Account getAccountByPesel(String pesel);
    Account createAccount(Account account);
}
