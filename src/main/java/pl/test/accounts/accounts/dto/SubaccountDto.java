package pl.test.accounts.accounts.dto;

import lombok.Getter;
import lombok.Setter;
import pl.test.accounts.accounts.dto.common.BaseDto;
import pl.test.accounts.accounts.enumeration.Currency;

import java.math.BigDecimal;

@Getter
@Setter
public class SubaccountDto extends BaseDto {
    private Currency currencyCode;
    private BigDecimal balance;
}
