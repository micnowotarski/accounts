package pl.test.accounts.accounts.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.test.accounts.accounts.domain.Account;
import pl.test.accounts.accounts.domain.Subaccount;
import pl.test.accounts.accounts.dto.CreateAccountDto;
import pl.test.accounts.accounts.enumeration.Currency;
import pl.test.accounts.accounts.mapper.AccountMapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AccountFactory {

    @Autowired
    private AccountMapper accountMapper;

    public Account create(CreateAccountDto createAccountDto){
        Account newAccount = accountMapper.map(createAccountDto);

        List<Subaccount> subaccounts = new ArrayList<>();
        subaccounts.add(new Subaccount(Currency.PLN, createAccountDto.getPlnInitialBalance()));

        List<Subaccount> foreignCurrencies = Arrays.stream(Currency.values())
                .filter(c -> !c.equals(Currency.PLN))
                .map(c -> new Subaccount(c, new BigDecimal(0))).collect(Collectors.toList());

        subaccounts.addAll(foreignCurrencies);
        newAccount.setSubaccounts(subaccounts);

        return newAccount;
    }
}
