package pl.test.accounts.accounts.dto;

import lombok.Getter;
import lombok.Setter;
import pl.test.accounts.accounts.dto.common.BaseDto;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
public class CreateAccountDto extends BaseDto {
    @NotEmpty
    private String name;
    @NotEmpty
    private String surname;
    @Size(min = 11, max = 11)
    private String pesel;
    @DecimalMin(value = "0.00", inclusive = true)
    private BigDecimal plnInitialBalance;
}
