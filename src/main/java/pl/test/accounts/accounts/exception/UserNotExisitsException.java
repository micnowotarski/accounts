package pl.test.accounts.accounts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="User not exisits")
public class UserNotExisitsException extends RuntimeException{
}
