package pl.test.accounts.accounts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="Account exists in database or is not an adult")
public class CantCreateAccountException extends RuntimeException{
}
